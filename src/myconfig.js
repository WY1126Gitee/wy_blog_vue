// 应用全局配置
module.exports = {
    baseUrl: 'http://localhost:8081',
    devUrl: 'http://localhost:8080',
    // 应用信息
    appInfo: {
        // 应用名称
        name: "Blog-app",
        // 应用版本
        version: "1.1.0",
        // 应用logo
        logo: "https://acimages-1302835316.cos.ap-shanghai.myqcloud.com/upload/20240619/wylogo.png",

        // githubUrl
        githubUrl: "https://gitee.com/WY1126Gitee/wy_blog_java.git",

        QQNumber: "1770624149",

        // // 官方网站
        // site_url: "http://ruoyi.vip",
        // // 政策协议
        // agreements: [{
        //   title: "隐私政策",
        //   url: "https://ruoyi.vip/protocol.html"
        // },
        //   {
        //     title: "用户服务协议",
        //     url: "https://ruoyi.vip/protocol.html"
        //   }
        // ]
    },
    // 存储桶信息
}
