import request from '@/utils/request'

// 注册方法
export function register(username, password) {
    const data = {
        username,
        password
    }
    return request({
        'url': '/user/register',
        headers: {
            isToken: false
        },
        'method': 'post',
        'data': data
    })
}


// 登录方法
export function login(username, password) {
    const data = {
        username,
        password
    }
    return request({
        'url': '/user/login',
        headers: {
            isToken: false
        },
        'method': 'post',
        'data': data
    })
}

// 获取用户详细信息
export function getInfo() {
    return request({
        'url': '/user/getUserInfo',
        'method': 'get'
    })
}

export function updateUserInfo(data) {
    return request({
        'url': '/user/updateUserInfo',
        'method': 'post',
         data
    })
}
//  校验旧密码
export function checkOldPassword(data) {
    return request({
        url: '/user/checkOldPassword',
        method: 'post',
        data
    })
}

//  更改密码
export function changePassword(data) {
    return request({
        url: '/user/changePassword',
        method: 'post',
        data
    })
}


// 退出方法
export function logout() {
    return request({
        'url': '/user/logout',
        'method': 'post'
    })
}

// 校验token
export function isTokenDead() {
    return request({
        'url': '/user/isTokenDead',
        'method': 'post'
    })
}

// 检查用户名是否重复
export function checkUsername(username) {
    return request({
        'url': '/user/checkUsername',
        'method': 'get',
        params: {username}
    })
}

// 获取验证码
// export function getCodeImg() {
//   return request({
//     'url': '/captchaImage',
//     headers: {
//       isToken: false
//     },
//     method: 'get',
//     timeout: 20000
//   })
// }
