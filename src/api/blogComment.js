import request from '@/utils/request'



// 获取最新留言
export function getLastLeaveWord() {
    return request({
        'url': '/blogComment/getLastLeaveWord',
        'method': 'get'
    })
}

export function addBlogComment( blogCommentForm ) {
    return request({
        'url': '/blogComment/addBlogComment',
        'method': 'post',
        'data': blogCommentForm
    })
}

// 获取所有博客分页
export function getBlogCommentPage(queryList) {
    return request({
        'url': 'blogComment/getBlogCommentPage',
        'method': 'post',
        'data': queryList
    })
}
export function getAllBootCommentReply(queryList) {
    return request({
        'url': 'blogComment/getAllBootCommentReply',
        'method': 'post',
        'data': queryList
    })
}
export function doLikeComment(doLikeList) {
    return request({
        'url': '/blogComment/doLikeBlogComment',
        'method': 'post',
        'data': doLikeList
    })
}

// // 获取ID博客
// export function getBlogById(id, uid) {
//     return request({
//         'url': '/blog/getBlogById',
//         'method': 'get',
//         params: { id, uid }
//     })
// }
//
// // 获取热门博客
// export function getBlogHot() {
//     return request({
//         'url': '/blog/getBlogHot',
//         'method': 'get'
//     })
// }
// // 获取博客分类和数量
// export function getAllBlogClassNumber() {
//     return request({
//         'url': '/blog/getAllBlogClassNumber',
//         'method': 'get',
//     })
// }
//
//
// // 获取所有博客分页
// export function doLike(doLikeList) {
//     return request({
//         'url': '/blog/doLike',
//         'method': 'post',
//         'data': doLikeList
//     })
// }
//
// // test
// export function blogTest() {
//     return request({
//         'url': '/blog/blogTest',
//         'method': 'get'
//     })
// }
//
// // 登录方法
// export function login(username, password) {
//     const data = {
//         username,
//         password
//     }
//     return request({
//         'url': '/user/login',
//         headers: {
//             isToken: false
//         },
//         'method': 'post',
//         'data': data
//     })
// }
//
// // 获取用户详细信息
// export function getInfo() {
//     return request({
//         'url': '/user/getUserInfo',
//         'method': 'get'
//     })
// }
//
// // 退出方法
// export function logout() {
//     return request({
//         'url': '/user/logout',
//         headers: {
//             isToken: false
//         },
//         'method': 'post'
//     })
// }
