import request from '@/utils/request'


// 添加博客
// export function addBlog( uid, title, blogAbstract, contentMarkdown, contentHtml, createTime, likeNumber, cid, lid) {
export function addBlog( blogForm ) {

    // const data = {
    //     uid,
    //     title,
    //     blogAbstract,
    //     contentMarkdown,
    //     contentHtml,
    //     createTime,
    //     likeNumber,
    //     cid,
    //     lid,
    // }
    return request({
        'url': '/blog/addBlog',
        headers: {
            isToken: false
        },
        'method': 'post',
        'data': blogForm
    })
}

// 获取所有博客分类
export function getAllBlogClass() {
    return request({
        'url': '/blog/getAllBlogClass',
        'method': 'get',
    })
}

// 获取所有博客分页
export function getBlogPage(queryList) {
    return request({
        'url': '/blog/getBlogPage',
        'method': 'post',
        'data': queryList
    })
}

// 获取所有Notes
export function getLittleBlogPage(queryList) {
    return request({
        'url': '/blog/getLittleBlogPage',
        'method': 'post',
        'data': queryList
    })
}

// 获取ID博客
export function getBlogById(id, uid) {
    return request({
        'url': '/blog/getBlogById',
        'method': 'get',
        params: { id, uid }
    })
}

// 获取热门博客
export function getBlogHot() {
    return request({
        'url': '/blog/getBlogHot',
        'method': 'get'
    })
}
// 获取博客分类和数量
export function getAllBlogClassNumber() {
    return request({
        'url': '/blog/getAllBlogClassNumber',
        'method': 'get',
    })
}


// 获取所有博客分页
export function doLike(doLikeList) {
    return request({
        'url': '/blog/doLike',
        'method': 'post',
        'data': doLikeList
    })
}

// test
export function blogTest() {
    return request({
        'url': '/blog/blogTest',
        'method': 'get'
    })
}
//
// // 登录方法
// export function login(username, password) {
//     const data = {
//         username,
//         password
//     }
//     return request({
//         'url': '/user/login',
//         headers: {
//             isToken: false
//         },
//         'method': 'post',
//         'data': data
//     })
// }
//
// // 获取用户详细信息
// export function getInfo() {
//     return request({
//         'url': '/user/getUserInfo',
//         'method': 'get'
//     })
// }
//
// // 退出方法
// export function logout() {
//     return request({
//         'url': '/user/logout',
//         headers: {
//             isToken: false
//         },
//         'method': 'post'
//     })
// }
