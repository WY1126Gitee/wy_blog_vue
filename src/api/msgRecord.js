import request from '@/utils/request'


// 获取回复和评论消息
export function getSystemMsgPage(queryList) {
    return request({
        'url': 'msgRecord/getSystemMsgPage',
        'method': 'post',
        'data': queryList
    })
}

// 存储用户系统消息并清空队列
export function getUserSystemMsgAndClearQueue(queryList) {
    return request({
        'url': '/msgRecord/getUserSystemMsgAndClearQueue',
        'method': 'post',
        'data': queryList
    })
}

// 获取回复和评论消息
export function getReplyAndCommentPage(queryList) {
    return request({
        'url': '/msgRecord/getReplyAndCommentPage',
        'method': 'post',
        'data': queryList
    })
}
// 删除用户全部的回复和评论消息/msgRecord/deleteTargetUserAllReplyAndCommentMsg
export function deleteTargetUserAllMsgByType(targetUserId, type) {
    return request({
        'url': '/msgRecord/deleteTargetUserAllMsgByType',
        'method': 'get',
        params: { targetUserId, type }
    })
}
// export function getAllBootCommentReply(queryList) {
//     return request({
//         'url': 'blogComment/getAllBootCommentReply',
//         'method': 'post',
//         'data': queryList
//     })
// }

// 清除对应的Redis
export function clearKeyRedis(key, targetUserId) {
    return request({
        'url': '/msgRecord/clearKeyRedis',
        'method': 'get',
        params: { key, targetUserId }
    })
}

// /msgRecord/deleteOneMsgRecordById

// 删除某个
export function deleteOneMsgRecordById(id) {
    return request({
        'url': '/msgRecord/deleteOneMsgRecordById',
        'method': 'get',
        params: { id }
    })
}


// 获取回复和评论消息
export function getDoLikePage(queryList) {
    return request({
        'url': 'msgRecord/getDoLikePage',
        'method': 'post',
        'data': queryList
    })
}
//
// // 获取热门博客
// export function getBlogHot() {
//     return request({
//         'url': '/blog/getBlogHot',
//         'method': 'get'
//     })
// }
// // 获取博客分类和数量
// export function getAllBlogClassNumber() {
//     return request({
//         'url': '/blog/getAllBlogClassNumber',
//         'method': 'get',
//     })
// }
//
//
// // 获取所有博客分页
// export function doLike(doLikeList) {
//     return request({
//         'url': '/blog/doLike',
//         'method': 'post',
//         'data': doLikeList
//     })
// }
//
// // test
// export function blogTest() {
//     return request({
//         'url': '/blog/blogTest',
//         'method': 'get'
//     })
// }
//
// // 登录方法
// export function login(username, password) {
//     const data = {
//         username,
//         password
//     }
//     return request({
//         'url': '/user/login',
//         headers: {
//             isToken: false
//         },
//         'method': 'post',
//         'data': data
//     })
// }
//
// // 获取用户详细信息
// export function getInfo() {
//     return request({
//         'url': '/user/getUserInfo',
//         'method': 'get'
//     })
// }
//
// // 退出方法
// export function logout() {
//     return request({
//         'url': '/user/logout',
//         headers: {
//             isToken: false
//         },
//         'method': 'post'
//     })
// }
