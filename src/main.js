import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import './permission' // permission control
import './permission'
import '@/icons'

/** MD 编辑器配置开始  */
import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';

// highlightjs
import hljs from 'highlight.js';

VMdEditor.use(vuepressTheme, {
  Hljs: hljs,
});

Vue.use(VMdEditor);
//------------------------------------------------

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
// use
Vue.use(mavonEditor)

import VMdPreview from '@kangc/v-md-editor/lib/preview';
import '@kangc/v-md-editor/lib/style/preview.css';
// import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';

// highlightjs
// import hljs from 'highlight.js';

VMdPreview.use(githubTheme, {
  Hljs: hljs,
});

Vue.use(VMdPreview);

// ----------------------------------------------------

/** MD 编辑器配置结束  */

//导入element-ui的依赖
import ElementUI  from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
//全局使用element-ui
Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$store = store

Vue.prototype.$onWait=new Promise((reslove)=>{
  Vue.prototype.$reslove=reslove;
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
