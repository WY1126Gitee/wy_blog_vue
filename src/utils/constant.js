const constant = {
    avatar: 'vuex_avatar',
    nickname: 'vuex_name',
    roles: 'vuex_roles',
    id: 'vuex_id',
    // permissions: 'vuex_permissions'
}

export default constant
