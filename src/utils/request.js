import axios from 'axios'
// import config from '@/vue.config'
// import { defineConfig } from "@vue/cli-service";
// import { baseUrl } from '../myconfig'
import myConfig from '@/myconfig'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
// import router from './router'
import {getToken, removeToken, setToken} from '@/utils/auth'
import router from "@/router";
const baseUrl = myConfig.baseUrl
// create an axios instance
const service = axios.create({
    // baseURL: process.env['VUE_APP_BASE_API'], // url = base url + request url
    // withCredentials: true, // send cookies when cross-domain requests
    baseURL: baseUrl,
    timeout: 3000 // request timeout
})


// request interceptor
service.interceptors.request.use(
    config => {
        // do something before request is sent

        if (store.getters.token) {
            // let each request carry token
            // ['X-Token'] is a custom headers key
            // please modify it according to the actual situation
            config.headers['token'] = getToken()
        }
        return config
    },
    error => {
        // do something with request error
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    /**
     * If you want to get http information such as headers or status
     * Please return  response => response
     */

    /**
     * Determine the request status by custom code
     * Here is just an example
     * You can also judge the status by HTTP Status Code
     */
    response => {

        // 刷新token
        // console.log(response[1]['header'].refreshtoken)
        // if (response[1]['header'].refreshtoken != null) {
        //     removeToken()
        //     setToken(response[1]['header'].refreshtoken)
        // }
        console.log("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", response)
        if ( response.headers.get('refreshToken') != null) {
            removeToken()
            setToken( response.headers.get('refreshToken'))
        }
        const res = response.data
        // const that = this


        // if the custom code is not 20000, it is judged as an error.
        if (res.code !== 20000) {
            Message({
                message: res.msg || 'Error',
                type: 'error',
                duration: 3 * 1000
            })

            // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
            if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
                // to re-login
                let msg = '登录信息已过期，请重新登录';
                let title = '信息过期';
                // if (res.code === 50008) {
                //     msg = "登录信息已过期，请重新登录"
                //     title = '信息过期'
                // } else {
                //     msg = "您已注销，可以取消以留在此页面，也可以重新登录"
                //     title = '确认注销'
                // }
                MessageBox.confirm(msg, title, {
                    confirmButtonText: '重新登录',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    store.dispatch('user/resetToken').then(() => {
                        // location.reload()
                        router.push('/login')
                        // this.$msgbox.close();
                        // location.reload()

                        // this.$router.push('/login').then(() => {})
                        // location.reload()
                    })
                }).catch(() => {
                    store.dispatch('user/resetToken').then(() => {
                        // 停留在此页面也要reload
                        // location.reload()
                        // 当前页面需要登录则跳转至首页
                        console.log("dsdadsdasdasda")
                        // console.log(router.history.current.meta.requiresAuth)
                        if (router.history.current.meta.requiresAuth) {
                            router.push('/home')
                            //     .then(() =>
                            //     location.reload()
                            // )
                            console.log("aaaa")
                        } else {
                            location.reload()
                        }
                    })

                })
            }
            return Promise.reject(new Error(res.msg || 'Error'))
        } else {
            return res
        }
    },
    error => {
        console.log('err' + error) // for debug
        Message({
            message: error.message,
            type: 'error',
            duration: 3 * 1000
        })
        return Promise.reject(error)
    }
)

export default service
