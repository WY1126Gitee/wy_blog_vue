import constant from './constant'
// import Cookies from 'js-cookie'

// 存储变量名
let storageKey = 'storage_data'

// 存储节点变量名
let storageNodeKeys = [constant.avatar, constant.nickname, constant.roles, constant.id]

let storageData  = sessionStorage.getItem(storageKey) || {}

const storage = {
    set: function(key, value) {
        if (storageNodeKeys.indexOf(key) !== -1) {
            // let tmp = uni.getStorageSync(storageKey)
            let tmp = sessionStorage.getItem(storageKey)
            tmp = tmp ? tmp : {}
            tmp[key] = value
            sessionStorage.setItem(storageKey, tmp)
            // uni.setStorageSync(storageKey, tmp)
        }
    },
    get: function(key) {
        return storageData[key] || ""
    },
    remove: function(key) {
        delete storageData[key]
        // sessionStorage.removeItem(storageKey)
        sessionStorage.setItem(storageKey, storageData)
        // uni.setStorageSync(storageKey, storageData)
    },
    clean: function() {
        sessionStorage.removeItem(storageKey)
        // uni.removeStorageSync(storageKey)
    }
}

export default storage
