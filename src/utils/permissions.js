import store from '@/store'

/**
 * 字符权限校验
 * @param {Array} value 校验值
 * @returns {Boolean}
 */
// export function checkPermi(value) {
//     if (value && value instanceof Array && value.length > 0) {
//         const permissions = store.getters && store.getters.permissions
//         const permissionDatas = value
//         const all_permission = "*:*:*"
//
//         const hasPermission = permissions.some(permission => {
//             return all_permission === permission || permissionDatas.includes(permission)
//         })
//
//         if (!hasPermission) {
//             return false
//         }
//         return true
//     } else {
//         console.error(`need roles! Like checkPermi="['system:user:add','system:user:edit']"`)
//         return false
//     }
// }

/**
 * 角色权限校验
 * @param {Array} value 校验值
 * @returns {Boolean}
 */
export function checkRole(value) {
    if (value && value instanceof Array && value.length > 0) {
        const roles = store.getters && store.getters.roles
        if (roles && roles.length <= 0) { return false }
        const permissionRoles = value
        const super_admin = "admin"

        // let arr = JSON.parse(roles)
        //
        // console.log('checkRole' + arr)
        // 去除字符串中的方括号，并使用逗号分割成数组
        let arr = roles.slice(1, -1).split(", ");
        console.log('checkRole' + roles)

        // console.log(arr)
        const hasRole = arr.some(role => {
            return super_admin === role || permissionRoles.includes(role)
        })

        if (!hasRole) {
            return false
        }
        return true
    } else {
        console.error(`need roles! Like checkRole="['admin','root']"`)
        return false
    }
}