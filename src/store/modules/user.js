import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'
// import { resetRouter } from '@/router'
// import myConfig from 'myconfig'
import Cookies from 'js-cookie'
// import cookies from "@/utils/storage";
// import storage from "@/utils/storage";
// import constant from "@/utils/constant";


// const baseUrl = myConfig.baseUrl

const getDefaultState = () => {
    return {
        token: getToken(),
        nickname: Cookies.get('nickname'),
        avatar: Cookies.get('avatar'),
        roles: Cookies.get('roles'),
        id: Cookies.get('id'),
        isMsg: Cookies.get('isMsg'),
    }
}

const state = getDefaultState()

const mutations = {
    RESET_STATE: (state) => {
        Object.assign(state, getDefaultState())
        // state.roles = []
    },
    SET_TOKEN: (state, token) => {
        state.token = token
    },
    SET_NICKNAME: (state, nickname) => {
        state.nickname = nickname
        Cookies.set('nickname', nickname)
        // sessionStorage.setItem('nickname', nickname)
        // storage.set(constant.nickname, nickname)
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar
        Cookies.set('avatar', avatar)
        // sessionStorage.setItem('avatar', avatar)
        // storage.set(constant.avatar, avatar)
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles
        Cookies.set('roles', roles)
        // sessionStorage.setItem('roles', roles)
        // storage.set(constant.roles, roles)
    },
    SET_ID: (state, id) => {
        state.id = id
        Cookies.set('id', id)
        // sessionStorage.setItem('id', id)
        // storage.set(constant.id, id)
    },
    SET_ISMSG: (state, msg) => {
        state.isMsg = msg
        Cookies.set('isMsg', msg)
    }
}

const actions = {
    // user login
    login({ commit }, userInfo) {
        const { username, password } = userInfo
        return new Promise((resolve, reject) => {
            login(username.trim(), password ).then(response => {
                const { data } = response
                if (data.token != null && data.token != '') {
                    commit('SET_TOKEN', data.token)
                    setToken(data.token)
                }
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // get user info
    getInfo({ commit, state }) {
        return new Promise((resolve, reject) => {
            getInfo(state.token).then(response => {
                console.log("getInfo")
                const { data } = response
                console.log(data)
                if (!data) {
                    return reject('验证失败，请重新登录。')
                }

                const { nickname, avatar, id, roles, isMsg} = data

                commit('SET_NICKNAME', nickname)
                commit('SET_AVATAR', avatar)
                commit('SET_ID', id)
                commit('SET_ROLES', roles)
                commit('SET_ISMSG', isMsg)

                resolve(data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // user logout
    logout({ commit, state }) {

        // 再清除服务器，但可能token已经过期，选择停留再本页面还是
        return new Promise((resolve, reject) => {
            logout(state.token).then((res) => {
                console.log(res)
                // 首先清楚本地缓存
                removeToken() // must remove  token  first
                Cookies.remove('nickname')
                Cookies.remove('avatar')
                Cookies.remove('id')
                Cookies.remove('roles')
                Cookies.remove('isMsg')
                commit('RESET_STATE')
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // remove token
    resetToken({ commit }) {
        return new Promise(resolve => {
            removeToken() // must remove  token  first

            // storage.clean()
            Cookies.remove('nickname')
            Cookies.remove('avatar')
            Cookies.remove('id')
            Cookies.remove('roles')
            Cookies.remove('isMsg')
            // sessionStorage.removeItem('nickname')
            // sessionStorage.removeItem('avatar')
            // sessionStorage.removeItem('id')
            // sessionStorage.removeItem('roles')
            console.log(getToken())
            commit('RESET_STATE')
            resolve()
        })
    },
    updateUserInfo({ commit }, userInfo) {
        return new Promise((resolve) => {
            Cookies.set('nickname', userInfo.nickname)
            Cookies.set('avatar', userInfo.avatar)
            Cookies.set('id', userInfo.id)
            commit('RESET_STATE')
            resolve()
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}

