const getters = {
    token: state => state.user.token,
    avatar: state => state.user.avatar,
    nickname: state => state.user.nickname,
    roles: state => state.user.roles,
    id: state => state.user.id,
    isMsg: state => state.user.isMsg
}
export default getters
