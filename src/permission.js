/**
 * 请求登录验证，如果没有登录，不能访问页面，返回到登录页面
 */
import store from './store'
import router from "./router";
import {getToken} from "@/utils/auth";
import {Message} from "element-ui";
//路由判断登录，根据路由配置文件的参数
router.beforeEach(async (to, from, next) => {
    console.log('permission')
    // set page title
    // document.title = getPageTitle(to.meta.title)
    //判断该路由是否需要登录权限
    //record.meta.requireAuth是获取到该请求中携带的该参数
    const token = getToken();
    if (to.matched.some(record => record.meta.requiresAuth)) {
        //获取到本地的token
        console.log("显示token----------：" + token)

        //判断当前的token是否存在，也就是登录时的token
        if (token) {
            console.log("hastoken")
            //如果指向的是登录页面，不做任何操作
            // eslint-disable-next-line no-empty
            if (to.path === "/login") {
                // await store.dispatch('user/resetToken')
                next({path: '/'})
            } else {
                // 获取用户信息
                const hasGetUserInfo = store.getters.nickname
                if (hasGetUserInfo) {
                    next()
                } else {
                    try {
                        await store.dispatch('user/getInfo')
                        next()
                    } catch (error) {
                        await store.dispatch('user/resetToken')
                        Message.error(error || 'Has Error')
                        next({path: '/login'})
                        // NProgress.done()
                    }
                }
                //如果不是登录页面，且token存在，就放行
                // next()
            }
        } else {
            //    如果token不存在
            //    前往登录
            next({path: '/login'})
        }

    } else {
        //如果不需要登录认证，就直接访问
        if (token && to.path === "/login") {
            // 如果有token还去往登录页面就重定向到home
            next({path: '/'})
        } else {
            next()
        }
    }
})