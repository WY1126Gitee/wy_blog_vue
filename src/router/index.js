import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
// import myconfig from '../../myconfig'
import Layout from '@/layout'
import AboutView from "@/views/AboutView.vue";
import BlogView from "@/views/BlogView.vue";
import BlogDetail from "@/views/BlogDetail.vue";
import NotesView from "@/views/NotesView.vue";
Vue.use(VueRouter)
/**
 * keepAlive表示是否需要缓存页面
 *
 */

const routes = [
  {
    path: '/login',
    name: 'MyLogin',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue'),
    meta: {
      // requiresAuth: true,       // 判断是否需要登录
      // keepAlive: false,
    },
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [{
      path: 'home',
      name: 'Home',
      component: HomeView,
      meta: {
        keepAlive: true,
        title: "sss"
      }
    }],
  },
  {
    path: '/about',
    component: Layout,
    // redirect: '/about',
    children: [{
      path: 'index',
      name: 'About',
      component: AboutView,
      meta: {
        requiresAuth: true,       // 判断是否需要登录
        // keepAlive: true,          // 判断是否需要显示APP.vue
      }
    }]
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),

  },
  {
    path: '/notes',
    component: Layout,
    // redirect: '/about',
    children: [{
      path: 'index',
      name: 'Notes',
      component: NotesView,
      meta: {
        // requiresAuth: true,       // 判断是否需要登录
        // keepAlive: true,          // 判断是否需要显示APP.vue
      }
    }]

  },
  {
    path: '/blogView',
    // name: 'BlogView',
    component: Layout,
    children: [{
      path: 'index',
      name: 'BlogView',
      component: BlogView,
      meta: {
        title: 'blogview'
        // keepAlive: true,
      }
    }]
  },
  {
    path: '/blogDetail',
    // name: 'BlogView',
    component: Layout,
    children: [{
      path: 'index/:id',
      name: 'BlogDetail',
      component: BlogDetail,
      meta: {
        title: 'blogDetail'
        // keepAlive: true,
      }
    }]
  },
  {
    path: '/hot',
    component: Layout,
    // redirect: '/md',
    children: [{
      path: 'index',
      name: 'HotView',
      component: () => import( '../views/HotView.vue'),
      meta: {
        title: 'HotView',
        // keepAlive: true,    // 是否需要显示顶部
      }
    }]
  },
  {
    path: '/leaveWord',
    component: Layout,
    // redirect: '/md',
    children: [{
      path: 'index',
      name: 'LeaveWordView',
      component: () => import( '../views/LeaveWordView.vue'),
      meta: {
        title: 'LeaveWordView',
        // keepAlive: true,    // 是否需要显示顶部
      }
    }]
  },
  {
    path: '/msg',
    component: Layout,
    // redirect: '/md',
    children: [{
      path: 'index',
      name: 'MessageView',
      component: () => import( '../views/MessageView.vue'),
      meta: {
        title: 'MessageView',
        requiresAuth: true,       // 判断是否需要登录
        // keepAlive: true,    // 是否需要显示顶部
      }
    }]
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.

  },
  {
    path: '/person',
    component: Layout,
    // redirect: '/md',
    children: [
      {
        path: 'index',
        name: 'PersonView',
        component: () => import( '../views/PersonView.vue'),
        meta: {
          requiresAuth: true,       // 判断是否需要登录
          title: 'PersonView',
          // keepAlive: true,    // 是否需要显示顶部
        }
      }, {
        path: 'system',
        name: 'SystemView',
        component: () => import( '../views/SystemView.vue'),
        meta: {
          requiresAuth: true,       // 判断是否需要登录
          title: 'SystemView',
          // keepAlive: true,    // 是否需要显示顶部
        }
      },
    ]
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.

  },
  {
    path: '/postBlog',
    name: 'PostBlog',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/PostBlog.vue'),
    meta: {
      requiresAuth: true,       // 判断是否需要登录
      // keepAlive: false,     // 是否需要APP.vue
    }
  },
  {
    path: '/Share/toWechat/:id',
    name: 'toWechat',
    component: () => import(/* webpackChunkName: "about" */ '../views/ToWechat.vue'),
    meta: {
      // requiresAuth: true,       // 判断是否需要登录
      // keepAlive: false,
    }
  },
  {
    path: '/test',
    name: 'Test',
    component: () => import(/* webpackChunkName: "about" */ '../views/Test.vue'),
    meta: {
      keepAlive: false,       // 单独一页
    }
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
